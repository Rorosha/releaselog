# This is the header you want to show up at the top of your
# release notes.
RELNOTES_HEADER = """.. _release-notes-for-taskotron:

===========================
  Taskotron Release Notes
===========================

These are the release notes for the current stable release
of taskotron.


"""

FRAGMENT_HEADER = '**%s %s**\n'

# This is the format string for log entries returned from
# `git log --pretty=format:<foo>`
FMT_STRING = ' * %s (`%h <http://phab.qadevel.cloud.fedoraproject\
.org/rLTRN%H>`_)%n'
